using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInterfaceManager : MonoBehaviour
{
    [SerializeField]
    private GameObject menuButton;

    [SerializeField]
    private Text score;

    [SerializeField]
    private Transform menu;

    [SerializeField]
    private Transform gameOverMenu;

    [SerializeField]
    private Transform startMenu;

    [SerializeField]
    private GameManager gameManager;

    private bool isRestarting = false;

    void Start()
    {
        gameManager.LevelLogic.OnGameOver += OnGameOver;
        gameManager.LevelLogic.OnScoreChange += LevelLogic_OnScoreChange;
    }

    void Update()
    {
        
    }

    public void OnMenuNewGameButtonPressed()
    {
        Time.timeScale = 1;
        menu.gameObject.SetActive(false);
        isRestarting = true;
        gameManager.LevelLogic.GameOver();
    }

    public void OnStartMenuNewGameButtonPressed()
    {
        gameManager.LevelLogic.StartLevel();
        startMenu.gameObject.SetActive(false);
    }

    public void OnGameOverMenuNewGameButtonPressed()
    {
        gameManager.LevelLogic.StartLevel();
        gameOverMenu.gameObject.SetActive(false);
    }

    private void OnGameOver()
    {
        if(isRestarting)
        {
            isRestarting = false;
            gameManager.LevelLogic.StartLevel();
            return;
        }
        score.text = "0";
        gameOverMenu.gameObject.SetActive(true);

    }

    public void OnExitButtonPressed()
    {
        Application.Quit();
    }

    public void OnMenuButtonPressed()
    {
        menu.gameObject.SetActive(true);
        Time.timeScale = 0;
    }
    public void OnResumeButtonPressed()
    {
        menu.gameObject.SetActive(false);
        Time.timeScale = 1;
    }




    private void LevelLogic_OnScoreChange(int value)
    {
        score.text = value.ToString();
    }

    private void OnDestroy()
    {
        gameManager.LevelLogic.OnGameOver -= OnGameOver;
        gameManager.LevelLogic.OnScoreChange -= LevelLogic_OnScoreChange;
    }

}
