using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Collider))]
public class ClickDamageReceiver : MonoBehaviour
{
    [SerializeField]
    private Unit damageableUnit;

    private DamageableBehaviour DamageableBehaviour;
    private DamagingBehaviour playerClickDamage;

    private void Start()
    {
        Player player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        DamageableBehaviour = damageableUnit.DamageableBehaviour;
        playerClickDamage = player.DamagingBehaviour;
    }

    private void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        DamageableBehaviour.TakeDamage(playerClickDamage.GetDamage(), playerClickDamage.DamageType);
    }
}
