using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraConroller : MonoBehaviour
{
    [SerializeField]
    float cameraSpeed = 10f;

    [SerializeField]
    GameBoard gameBoard;

    bool isSwipe = false;

    Vector2 firstTouchPos;

    private Vector3 targetPos;
    private float speed = 1f;
    private float neededSpeed = 1f;

    void Start()
    {
        targetPos = transform.position;
    }

    void Update()
    {
        int touchCount = Input.touchCount;

        if (touchCount > 0)
        {

            Touch touch = Input.GetTouch(0);


            if (!isSwipe)
            {
                firstTouchPos = touch.position;
                isSwipe = true;
            }


            Vector2 vec = touch.position - firstTouchPos;
            float dist = vec.magnitude;

            var phase = touch.phase;

            if(isSwipe && dist > 150)
            {
                MoveCamera(touch.position);
            }

            isSwipe = !touch.phase.HasFlag(TouchPhase.Ended);

        }

        if(!isSwipe)
        {
            neededSpeed = 1f;
        }
        speed = Mathf.Lerp(speed, neededSpeed, cameraSpeed * Time.deltaTime);
        transform.position = Vector3.Lerp(transform.position, targetPos, speed);
    }

    private void MoveCamera(Vector2 touchPos)
    {
        Vector2 vec = touchPos - firstTouchPos;

        float dist = vec.magnitude;

        neededSpeed = dist/10;

        Vector2 dir = vec.normalized * Time.deltaTime * dist;

        targetPos = new Vector3(dir.x * 2, 0, dir.y);

        targetPos += transform.position;

        targetPos = new Vector3(Mathf.Clamp(targetPos.x, -gameBoard.BoardSize.x -gameBoard.BoardSize.x * 0.1f, gameBoard.BoardSize.x + gameBoard.BoardSize.x * 0.1f),
            targetPos.y, Mathf.Clamp(targetPos.z, -gameBoard.BoardSize.y - gameBoard.BoardSize.y * 0.1f, gameBoard.BoardSize.y + gameBoard.BoardSize.y * 0.1f));

    }
}
