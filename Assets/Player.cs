using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField][Min(0)]
    private int damage = 1;

    SimpleDamage DamageType;

    public DamagingBehaviour DamagingBehaviour { get; private set; }

    private void Awake()
    {
        DamagingBehaviour = new PlayerClickDamaging(damage, DamageType);
    }
}
