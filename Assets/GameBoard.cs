using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.TerrainAPI;
using UnityEngine;

[ExecuteInEditMode]
public class GameBoard : MonoBehaviour
{
    //� ���� ������ ����� ���������������� ������� �������� ������ � ����� ����������� � ����� ������ ������� ������������

    [SerializeField]
    private Vector2 boardSize;
    [SerializeField]
    private Transform boardGround;

    [SerializeField]
    private Transform enemyContainer;

    public Transform EnemyContainer
    {
        get
        {
            return enemyContainer;
        }
        private set
        {
            enemyContainer = value;
        }
    }


    public Vector2 BoardSize 
    {
        get
        {
            return boardSize;
        } 
        private set
        {
            boardSize = value;
        }
    }

    void Start()
    {
        boardGround.localScale = new Vector3(boardSize.x, 1, boardSize.y);
    }

    // Update is called once per frame
    void Update()
    {
        
        boardGround.localScale = new Vector3(boardSize.x, 1, boardSize.y);
    }

    
}
