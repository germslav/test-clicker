using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClickDamaging : DamagingBehaviour
{
    public PlayerClickDamaging(int damage, Damage damageType) : base(damage, damageType)
    {
        
    }

    public override void TryDoDamage(DamageableBehaviour damageableBehaviour = null)
    {
        base.TryDoDamage(damageableBehaviour);

    }
    
}
