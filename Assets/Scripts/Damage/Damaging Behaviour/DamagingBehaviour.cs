using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DamagingBehaviour
{
    public Damage DamageType { get; private set; }

    private int damage;

    public delegate void DamageChangeHandler(int value);
    public event DamageChangeHandler DamageChanged;

    public delegate void OnDamagingHandler(int value, Damage damageType);
    public event OnDamagingHandler OnDamaging;

    public DamagingBehaviour(int damage, Damage damageType)
    {
        this.damage = damage;
        DamageType = damageType;

    }

    public void ChangeDamage(int changingValue)
    {
        int newDamage = damage + changingValue;
        SetDamage(newDamage);
    }

    public void SetDamage(int damageValue)
    {
        damage = damageValue;
        DamageChanged?.Invoke(damage);
    }

    public int GetDamage()
    {
        return damage;
    }

    public virtual void TryDoDamage(DamageableBehaviour damageableBehaviour)
    {
        OnDamaging?.Invoke(damage, DamageType);
    }
}
