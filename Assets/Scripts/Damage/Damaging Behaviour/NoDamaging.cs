using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoDamaging : DamagingBehaviour
{
    public NoDamaging(int damage, Damage damageType) : base(damage, damageType)
    {

    }

    public override void TryDoDamage(DamageableBehaviour damageableBehaviour = null)
    {
        throw new System.NotImplementedException();
    }
}
