using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMeleeUnitDamageable : DamageableBehaviour
{
    public SimpleMeleeUnitDamageable(Health health) : base(health)
    {

    }

    public override void TakeDamage(int damage, Damage damageType)
    {
        base.TakeDamage(damage, damageType);
        health.ChangeHealth(-damage);
    }

}
