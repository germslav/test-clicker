using UnityEngine;


public abstract class DamageableBehaviour
{
    protected Health health;

    private Unit unit;

    public delegate void OnDamagedHandler(int value);
    public event OnDamagedHandler OnDamaged;


    public DamageableBehaviour(Health health)
    {
        this.health = health;
    }

    public virtual void TakeDamage(int damage, Damage damageType)
    {
        OnDamaged?.Invoke(damage);
    }

}
