using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DamageModificator : Damage
{
    private Damage damageComponent;

    public DamageModificator(Damage damage)
    {
        damageComponent = damage;
        DamageModificators.Add(this);
    }

    public override void DoDamage(int damage, DamageableBehaviour damageableBehaviour)
    {
        damageComponent.DamageModificators = DamageModificators;
        damageComponent.DoDamage(damage, damageableBehaviour);
    }
}
