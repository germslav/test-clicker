using System.Collections.Generic;

public abstract class Damage
{
    public int DamageValue { get; protected set; }
    public List<DamageModificator> DamageModificators;
    public abstract void DoDamage(int damage, DamageableBehaviour damageableBehaviour);
}
