
public class SimpleDamage : Damage
{

    public SimpleDamage(){}

    public override void DoDamage(int damage, DamageableBehaviour damageableBehaviour)
    {
        damageableBehaviour.TakeDamage(damage, this);
    }
}
