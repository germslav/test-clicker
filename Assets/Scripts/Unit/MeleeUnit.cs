using Game.MovingsBehaviour;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeUnit : Unit
{
    [SerializeField]
    protected float rotateSpeed;
    [SerializeField]
    protected Animator animator;

    public override void Initialize(int maxHealth, int damage, float speed, Attitude attitude, GameBoard gameBoard)
    {
        base.Initialize(maxHealth, damage, speed, attitude, gameBoard);

        damageType = new SimpleDamage();
        DamagingBehaviour = new NoDamaging(damage, damageType);
        DamageableBehaviour = new SimpleMeleeUnitDamageable(Health);
        MovingBehaviour = new WalkingUnit(transform, speed, behaviourRadius, rotateSpeed);
        AIBehaviour = new SimpleAIBehaviour(transform, gameBoard);

        Health.OnHealthOver += OnHealthOver;

    }

    private void OnHealthOver()
    {
        OnDead();
    }

    protected override void OnDead()
    {
        base.OnDead();
        animator.SetBool("Dead", true);

        StartCoroutine(DieProcess());
    }

    protected override void Die()
    {
        Destroy(gameObject);
    }

    protected IEnumerator DieProcess()
    {
        bool isReady = false;
        while (!isReady) 
        {
            isReady = animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1 
                && animator.GetCurrentAnimatorStateInfo(0).IsName("Dead");

            yield return null;
        }
        Die();
    }
}
