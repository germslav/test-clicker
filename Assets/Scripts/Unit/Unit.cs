using UnityEngine;
using Game.MovingsBehaviour;


public abstract class Unit : MonoBehaviour
{
    [SerializeField][Min(1)]
    protected int maxHealth;
    [SerializeField]
    protected Attitude attitude;
    [SerializeField][Min(0)]
    protected int damage;
    [SerializeField][Min(0)]
    protected float speed;
    [SerializeField]
    protected float behaviourRadius;

    protected bool isAlreadyInit = false;
    protected Damage damageType;

    public enum Attitude
    {
        Enemy,
        Ally,
        Neutral
    }

    //����� ����� ���� ����������� ����� ���������

    public int DamageValue
    {
        get
        {
            return damage;
        }
        private set
        {
            damage = value;
        }
    }
    public int HealthValue
    {
        get
        {
            return maxHealth;
        }
        private set
        {
            maxHealth = value;
        }
    }
    public float SpeedValue
    {
        get
        {
            return speed;
        }
        private set
        {
            speed = value;
        }
    }


    public GameBoard GameBoard { get; protected set; }

    public Attitude CurrentAttitude { get; protected set; }
    public Health Health { get; protected set; }
    //public Player Player { get; protected set; }
    public MovingBehaviour MovingBehaviour { get; protected set; }
    public DamagingBehaviour DamagingBehaviour { get; protected set; }
    public DamageableBehaviour DamageableBehaviour { get; protected set; }
    public AIBehaviour AIBehaviour { get; protected set; }


    public delegate void OnDeadHandler(GameObject unit);
    public event OnDeadHandler OnDeadEvent;


    protected virtual void Awake()
    {
        if(isAlreadyInit)
        {
            return;
        }

        Initialize(maxHealth, damage, speed, attitude);
    }

    protected virtual void Update()
    {
        AIBehaviour.LogicTick();
        MovingBehaviour.LogicTick();
    }

    //���� ������������� �������� � ������� ��� ���-�� �� ����
    public virtual void Initialize(int maxHealth,int damage, float speed, Attitude attitude, GameBoard gameBoard)
    {
        this.maxHealth = maxHealth;
        this.attitude = attitude;

        GameBoard = gameBoard;

        Health = new Health(maxHealth);

        CurrentAttitude = attitude;

        isAlreadyInit = true;

    }


    //���� ������������� ����� Awake
    private void Initialize(int maxHealth, int damage, float speed, Attitude attitude)
    {
        GameBoard gameBoard = GameObject.FindGameObjectWithTag("GameBoard").GetComponent<GameBoard>();
        Initialize(maxHealth, damage, speed, attitude, gameBoard);
    }

    public virtual void SetAttitude(Attitude value)
    {
        CurrentAttitude = value;
    }

    protected virtual void OnDead()
    {
        OnDeadEvent?.Invoke(gameObject);
    }

    protected abstract void Die();
}
