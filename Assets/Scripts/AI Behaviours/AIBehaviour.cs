using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIBehaviour
{
    protected Transform aiObject;
    protected GameBoard gameBoard;

    public AIBehaviour(Transform aiObj, GameBoard gameBoard) 
    {
        this.aiObject = aiObj;
        this.gameBoard = gameBoard;
    }
    public abstract void LogicTick();
}
