using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.MovingsBehaviour;

public class SimpleAIBehaviour : AIBehaviour
{
    MovingBehaviour movingBehaviour;
    DamageableBehaviour damageableBehaviour;

    private Vector3 randomTargetPos;

    private bool cantMove = false;
    public SimpleAIBehaviour(Transform aiObj, GameBoard gameBoard) : base(aiObj, gameBoard)
    {
        Unit unit = aiObj.GetComponent<Unit>();
        
        movingBehaviour = unit.MovingBehaviour;
        damageableBehaviour = unit.DamageableBehaviour;
        damageableBehaviour.OnDamaged += DamageableBehaviour_OnDamaged;
        unit.Health.OnHealthOver += Health_OnHealthOver;
    }

    ~SimpleAIBehaviour()
    {
        damageableBehaviour.OnDamaged -= DamageableBehaviour_OnDamaged;
    }

    public override void LogicTick()
    {
        if(movingBehaviour.IsMoving || cantMove)
        {
            return;
        }

        randomTargetPos = FindTargetPos();

        movingBehaviour.StartMove(randomTargetPos);
    }


    private void DamageableBehaviour_OnDamaged(int value)
    {

        randomTargetPos = FindTargetPos();
        movingBehaviour.StartMove(randomTargetPos);
    }

    private Vector3 FindTargetPos()
    {
        int x = (int)gameBoard.BoardSize.x;
        int y = (int)gameBoard.BoardSize.y;

        int randomX = Random.Range(-x, x);
        int randomY = Random.Range(-y, y);
        return new Vector3(randomX, 0, randomY);
    }

    private void Health_OnHealthOver()
    {
        movingBehaviour.StopMove();
        cantMove = true;
    }
}
