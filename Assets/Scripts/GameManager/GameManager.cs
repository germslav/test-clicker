using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private LevelConfiguration levelConfiguration;
    [SerializeField]
    private GameBoard gameBoard;

    public LevelConfiguration LevelConfiguration
    {
        get
        {
            return levelConfiguration;
        }
        private set
        {
            levelConfiguration = value;
        }
    }

    public LevelBuilder LevelBuilder { get; private set; }
    public LevelLogic LevelLogic { get; private set; }

    private void Awake()
    {
        LevelBuilder = levelConfiguration.GetLevelBuilder(gameBoard);
        LevelLogic = levelConfiguration.GetLevelLogic(gameBoard);

        LevelLogic.OnGameOver += OnGameOver;
    }

    private void Update()
    {
        LevelLogic.LogicUpdate();
    }

    public void OnGameOver()
    {

    }

   
}
