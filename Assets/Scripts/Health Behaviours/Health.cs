using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health
{
    private int currentHealth;

    public delegate void HealthChangedHandler(int value);
    public event HealthChangedHandler HealthChanged;

    public delegate void OnHealthOverHandler();
    public event OnHealthOverHandler OnHealthOver;

    public int MaxHealth { get; private set; }

    public Health(int maxHealth)
    {
        MaxHealth = maxHealth;
        currentHealth = maxHealth;
    }

    public int GetHealth()
    {
        return currentHealth;
    }

    public void SetHealth(int value)
    {
        currentHealth = value;
        HealthChanged?.Invoke(currentHealth);
        if(value <= 0)
        {
            OnHealthOver?.Invoke();
        }
    }

    public void ChangeHealth(int changingValue)
    {
        currentHealth += changingValue;
        HealthChanged?.Invoke(currentHealth);

        if (currentHealth <= 0)
        {
            OnHealthOver?.Invoke();
        }
    }

    public void SetMaxHealth(int healthValue)
    {
        if (healthValue <= 0)
        {
            return;
        }
        MaxHealth = healthValue;
        currentHealth = healthValue;
    }
}
