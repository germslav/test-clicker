using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Unit))]
public class DamageableView : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    private Unit unit;

    private DamageableBehaviour damageableBehaviour;

    private void Start()
    {
        unit = GetComponent<Unit>();
        damageableBehaviour = unit.DamageableBehaviour;
        damageableBehaviour.OnDamaged += OnDamaged;
    }

    private void OnDisable()
    {
        damageableBehaviour.OnDamaged -= OnDamaged;
    }

    private void OnDamaged(int damageValue)
    {
        animator.SetTrigger("TakeDamage");
    }
    
}
