using UnityEngine;

namespace Game.MovingsBehaviour
{
    public class WalkingUnit : MovingBehaviour
    {
        private float rotateSpeed = 10f;

        protected float behaviourRadius;

        public WalkingUnit(Transform transform, float speed, float behaviourRadius, float rotateSpeed) : base(transform)
        {
            this.speed = speed;
            this.behaviourRadius = behaviourRadius;
            this.rotateSpeed = rotateSpeed;
        }
    

        protected override void Move()
        {
            if (!IsMoving)
            {
                return;
            }

            float dist = (targetPosition - transform.position).magnitude;


            if (dist <= behaviourRadius)
            {
                StopMove();
            }

            Vector3 targetDir = targetPosition - transform.position;

            Quaternion rotTarget = Quaternion.LookRotation(targetDir);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, rotTarget, rotateSpeed * Time.deltaTime);;

            transform.Translate(transform.forward * Time.deltaTime * speed, Space.World);
        }
    }
}
