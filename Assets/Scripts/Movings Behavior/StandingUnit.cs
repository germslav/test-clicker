using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game.MovingsBehaviour{
    public class StandingUnit : MovingBehaviour
    {
        public StandingUnit(Transform transform) : base(transform)
        {
            SetSpeed(0f);
            StopMove();
        }

        
        protected override void Move()
        {
            throw new System.NotImplementedException();
        }

    }
}
