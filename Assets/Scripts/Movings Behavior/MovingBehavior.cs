using UnityEngine;

namespace Game.MovingsBehaviour
{
    public abstract class MovingBehaviour
    {
        public bool IsMoving { get; private set; }
        
        protected float speed = 0f;

        protected Vector3 targetPosition;

        protected Transform transform;

        public MovingBehaviour(Transform transform)
        {
            this.transform = transform;

            IsMoving = false;
        }


        public virtual void LogicTick()
        {
            Move();
        }

        protected abstract void Move();

        public void SetSpeed(float value)
        {
            if (value < 0)
            {
                return;
            }
            speed = value;
        }

        public float GetSpeed()
        {
            return speed;
        }

        public void StopMove()
        {
            IsMoving = false;
        }

        public void StartMove(Vector3 moveTarget)
        {
            IsMoving = true;
            targetPosition = moveTarget;
        }

        public void ResumeMove()
        {
            if(targetPosition == null)
            {
                return;
            }
            IsMoving = true;
        }
    }
}
