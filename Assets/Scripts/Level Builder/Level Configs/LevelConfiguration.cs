using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level Configuration", menuName = "Level Builder/Level Configuration")]
public class LevelConfiguration : ScriptableObject
{
	[SerializeField]
	private LevelBuilders levelBuilderType;
	[SerializeField]
	private LevelsLogic levelLogicType;
	[SerializeField]
	private UnitsContainer unitsContainer;

	private LevelLogic levelLogic;
	private LevelBuilder levelBuilder;

	private enum LevelsLogic
	{
		SimpleLevelLogic
	}

	private enum LevelBuilders
	{
		SimpleLevelBuilder
	}

	public LevelBuilder GetLevelBuilder(GameBoard gameBoard)
	{
		if(levelBuilder != null)
		{
			return levelBuilder;
		}

		switch(levelBuilderType)
		{
			case LevelBuilders.SimpleLevelBuilder:
				
				levelBuilder = new SimpleLevelBuilder(unitsContainer, gameBoard);
				break;
		}

		return levelBuilder;
	}

	public LevelLogic GetLevelLogic(GameBoard gameBoard)
	{
		if(levelLogic != null)
		{
			return levelLogic;
		}

		if(levelBuilder == null)
		{
			GetLevelBuilder(gameBoard);
		}

		switch (levelLogicType)
		{
			case LevelsLogic.SimpleLevelLogic:
				levelLogic = new SimpleLevelLogic(gameBoard, levelBuilder);
				break;
		}

		return levelLogic;
	}


	//��� �� �������� ������, ���-�� ���� ����� ������� ������ �������� �������� ��������(��������� �������� ������, �� �� � ��)
	//������ ���� ������������ �������� ������ (����� ������)
}
