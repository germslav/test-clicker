using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Units Container", menuName = "Level Builder/Units Container")]
public class UnitsContainer : ScriptableObject
{
    public List<GameObject> EnemyUnits;
    public List<GameObject> AllyUnits;
    public List<GameObject> NeutralUnits;
}
