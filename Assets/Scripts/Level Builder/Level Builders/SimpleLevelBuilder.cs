using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SimpleLevelBuilder : LevelBuilder
{
    private float damageScaler = 0.005f;
    private float maxHealthScaler = 0.05f;
    private float speedScaler = 0.05f;

    private float intervalScaler = 0.002f;


    private float spawnIntervalMin = 10f;
    private float spawnIntervalMax = 10f;
    private float spawnInterval = 10f;
    private float currentIntervalTime = 8f;

    private float passedTime;

    public SimpleLevelBuilder(UnitsContainer unitsContainer, GameBoard gameBoard) : base(gameBoard, unitsContainer)
    {

    }


    public override void LogicUpdate(float passedTime)
    {
        Debug.Log(passedTime);
        if (IsPaused)
        {
            return;
        }    

        base.LogicUpdate(passedTime);

        this.passedTime = passedTime;

        currentIntervalTime += Time.deltaTime;
        Debug.Log(currentIntervalTime + ", " + spawnInterval);
        if (currentIntervalTime >= spawnInterval)
        {
            currentIntervalTime = 0f;

            Build();

            CalculateSpawnInterval();
            RandomizeInterval();
        }

    }

    protected override void Build()
    {
        int random = Random.Range(0, unitsContainer.EnemyUnits.Count);

        int randomPosX = Random.Range(-(int)gameBoard.BoardSize.x, (int)gameBoard.BoardSize.x);
        int randomPosZ = Random.Range(-(int)gameBoard.BoardSize.y, (int)gameBoard.BoardSize.y);

        Vector3 randomPos = new Vector3(randomPosX, 0, randomPosZ);

        var unitObj = unitsContainer.EnemyUnits[random];

        GameObject unitGameObj = GameObject.Instantiate(unitObj, gameBoard.EnemyContainer);

        unitGameObj.transform.position = randomPos;

        Unit unit = unitGameObj.GetComponent<Unit>();

        unit.DamagingBehaviour.SetDamage(CalculateDamage(unit.DamageValue));
        unit.Health.SetMaxHealth(CalculateHealth(unit.HealthValue));
        unit.MovingBehaviour.SetSpeed(CalculateSpeed(unit.SpeedValue));

        unit.OnDeadEvent += OnUnitDieInvoke;

        units.Add(unitGameObj, unit.CurrentAttitude);
        OnUnitBuildInvoke(unitGameObj);

    }

    private void RandomizeInterval()
    {
        spawnInterval = Random.Range(spawnIntervalMin, spawnIntervalMax);
    }

    public override void PauseBuild()
    {
        base.PauseBuild();
        Debug.Log("PAuse");
        IsPaused = true;
    }

    public override void ResumeBuild()
    {
        base.ResumeBuild();
        IsPaused = false;
    }

    public override void StopBuild()
    {
        base.StopBuild();
        IsPaused = true;
        for ( int i = 0; i < units.Count; )
        {
            var unitObj = units.Keys.ElementAt(i);
            unitObj.GetComponent<Unit>().Health.SetHealth(0);
            Debug.Log(i + ", count = " + units.Count);
        }

    }

    protected override void OnUnitDieInvoke(GameObject unit)
    {
        base.OnUnitDieInvoke(unit);
    }

    public override void StartBuild()
    {
        base.StartBuild();
        IsPaused = false;
    }

    private int CalculateDamage(int damage)
    {
        float result = damage + damage * damageScaler * passedTime;

        return (int)result;
    }

    private int CalculateHealth(int health)
    {
        float result = health + health * maxHealthScaler * passedTime;


        return (int)result;
    }

    private float CalculateSpeed(float speed)
    {
        float result = speed + speed * speedScaler * passedTime;

        if (result >= 20f)
        {
            return 20f;
        }

        return result;
    }

    private void CalculateSpawnInterval()
    {
        float result = spawnIntervalMax - spawnIntervalMax * intervalScaler * passedTime;


        if (result <= spawnIntervalMin)
        {
            spawnIntervalMax = spawnIntervalMin;
        }

        spawnIntervalMax = result;
    }

    protected void ResetValues()
    {
        spawnIntervalMin = 10f;
        spawnIntervalMax = 10f;
        spawnInterval = 10f;
        currentIntervalTime = 8f;

        passedTime = 0f;
}
}
