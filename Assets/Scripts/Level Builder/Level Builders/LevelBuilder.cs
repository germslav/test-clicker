using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LevelBuilder
{
    protected Dictionary<GameObject, Unit.Attitude> units;

    protected UnitsContainer unitsContainer;

    public delegate void OnUnitDieHandler(Unit.Attitude attitude);
    public event OnUnitDieHandler OnUnitDie;

    public delegate void OnUnitBuildHandler(Unit.Attitude attitude);
    public event OnUnitBuildHandler OnUnitBuild;

    protected GameBoard gameBoard;

    public bool IsPaused { get; protected set; }

    public LevelBuilder(GameBoard gameBoard, UnitsContainer unitsContainer)
    {
        units = new Dictionary<GameObject, Unit.Attitude>();
        this.gameBoard = gameBoard;
        this.unitsContainer = unitsContainer;
    }

    public virtual void LogicUpdate(float passedTime)
    {

    }

    public virtual void StartBuild()
    {

    }

    public virtual void StopBuild()
    {

    }

    public virtual void PauseBuild()
    {

    }

    public virtual void ResumeBuild()
    {

    }

    protected virtual void OnUnitDieInvoke(GameObject unit)
    {
        Unit.Attitude attitude;
        if(units.TryGetValue(unit, out attitude))
        {
            OnUnitDie?.Invoke(attitude);
        }
        units.Remove(unit);
    }

    protected void OnUnitBuildInvoke(GameObject unit)
    {
        Unit.Attitude attitude;
        if (units.TryGetValue(unit, out attitude))
        {
            OnUnitBuild?.Invoke(attitude);
        }
    }

    protected abstract void Build();

}
