using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LevelLogic
{
    protected LevelBuilder levelBuilder;

    public delegate void OnGameOverHandler();
    public event OnGameOverHandler OnGameOver;

    public delegate void OnScoreChangeHandler(int value);
    public event OnScoreChangeHandler OnScoreChange;

    public int Score { get; protected set; }
    public bool IsGameContinues { get; protected set; }

    public bool IsGameOver { get; protected set; }

    public LevelLogic(GameBoard gameBoard, LevelBuilder levelBuilder)
    {
        Score = 0;
        this.levelBuilder = levelBuilder;
        IsGameContinues = false;
        IsGameOver = false;
    }

    public abstract void LogicUpdate();

    protected void ChangeScore(int changeValue)
    {
        Score += changeValue;
        OnScoreChange?.Invoke(Score);
    }

    public virtual void StartLevel()
    {
        IsGameContinues = true;
        IsGameOver = false;
        levelBuilder.StartBuild();
    }

    public virtual void GameOver()
    {
        IsGameContinues = false;
        IsGameOver = true;
        levelBuilder.StopBuild();
        OnGameOver?.Invoke();
    }
}
