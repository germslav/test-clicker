using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleLevelLogic : LevelLogic
{
    private int currentEnemyCount = 0;
    private int EnemyCountToLose = 10;

    float passedTime = 0;

    public SimpleLevelLogic(GameBoard gameBoard, LevelBuilder levelBuilder) : base(gameBoard, levelBuilder)
    {
        levelBuilder.OnUnitDie += OnUnitDie;
        levelBuilder.OnUnitBuild += OnUnitBuild;
    }

    public override void LogicUpdate()
    {
        if(!IsGameContinues || IsGameOver)
        {
            return;
        }
        passedTime += Time.deltaTime;
        levelBuilder.LogicUpdate(passedTime);

    }


    public void OnUnitDie(Unit.Attitude attitude)
    {
        if(attitude == Unit.Attitude.Enemy)
        {
            ChangeScore(1);
        }
    }

    private void OnUnitBuild(Unit.Attitude attitude)
    {
        if (attitude == Unit.Attitude.Ally)
        {
            ChangeScore(1);
            return;
        }

        if (attitude == Unit.Attitude.Enemy)
        {
            currentEnemyCount++;
        }

        if (currentEnemyCount >= EnemyCountToLose)
        {
            GameOver();
        }

    }

    public override void GameOver()
    {
        levelBuilder.StopBuild();
        currentEnemyCount = 0;
        Score = 0;
        passedTime = 0;
        base.GameOver();
    }

    public override void StartLevel()
    {
        base.StartLevel();
    }

    ~SimpleLevelLogic()
    {
        levelBuilder.OnUnitDie -= OnUnitDie;
        levelBuilder.OnUnitBuild -= OnUnitBuild;
    }
}
